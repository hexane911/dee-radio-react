import './button.sass'

interface IButton {
  onClick?: any;
  children: string;
  className?: string;
}

const Button = ({ onClick, children, className }: IButton) => {
  return (
    <button className={`button ${className ? className : ""}`} 
    onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
