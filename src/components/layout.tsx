import { TFeed, TFeedItem } from '../types';
import Feed from './feed';
import Header from './header'
import './layout.sass'
import Songs from './songs';

const exampleFeedItem: TFeedItem = {
    title: "Title something",
    content: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus nemo in odio quidem, vel inventore, corrupti molestiae magni eum porro eos consequatur atque nulla saepe deleniti iste laborum ipsam omnis?`,
    imageUrl: "asdasd",
    link: "https://www.gnu.org",
  };
  
  const exampleFeed: TFeed = [exampleFeedItem, exampleFeedItem, exampleFeedItem];

  
const Layout = (children? : any) => {
    return (
        <div className="layout">
            <div className="layout__left">
                <Header/>
                <div className="layout__main">
                    <Feed feed={exampleFeed} className='layout__feed'/>
                    <Songs className='layout__songs'></Songs>
                </div>
            </div>
            <div className="layout__right"></div>
        </div>
    )
}

export default Layout