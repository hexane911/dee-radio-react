interface IPlayer {
    className?: string
}


const Player = ({className} : IPlayer) => {
    return <div className={`player ${className ? className : ''}`}>
        <div className="player__controls">
            controls
        </div>
    </div>
}

export default Player