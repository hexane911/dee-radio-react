import { TFeed, TFeedItem } from "../types";
import "./feed.sass";

interface IFeed  {
    feed : TFeed,
    className?: string
}

const Feed = ({feed, className} : IFeed) => {
  return (
    <div className={`feed ${className ? className : ""}`}>
      <div className="feed__inner">
        {feed.map((el) => {
          return (
            <div className="feed__item">
              <div className="feed__item-top">
                <div className="feed__item-image"></div>
                <div className="feed__item-meta">
                  <span className="feed__item-date">12 / 06 / 2022</span>
                  <span className="feed__item-date">12 / 06</span>
                  <h3 className="feed__item-title">{el.title}</h3>
                  <a href={el.link} className="feed__item-link">
                    {el.link}
                  </a>
                </div>
              </div>
                <p className="feed__item-content">
                    {el.content}
                </p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Feed;
