import './songs.sass'

interface ISongs {
    className?: string
}

const Songs = ({className} : ISongs) => {
    return <div className={`songs ${className ? className : ""}`}></div>
}

export default Songs