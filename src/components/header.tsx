import { useState } from "react";
import "./header.sass";
import logo from "../assets/images/cowdog-still.svg";
import Player from "./player";
import Button from "./button";

const Header = () => {
  const [view, setView] = useState<"full" | "half">("full");
  return (
    <header className="header">
      <div className="header__item">
        <div className="header__logo">
          <img src={logo} alt="" className="header__logo-img" />
          радио собака
        </div>
      </div>
      <div className="header__item">
        <Player className="header__player" />
      </div>
      <div className="header__item">
        <Button className="header__button">о нас</Button>
      </div>
    </header>
  );
};

export default Header;
