export type TFeedItem = {
    title? : string;
    content? : string;
    imageUrl? : string;
    link? : string
}

export type TFeed = TFeedItem[]